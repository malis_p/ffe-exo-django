import os
from .mode import *

try:
   from .settings.base_settings import *
except ImportError:
    raise Exception("BASE SETTINGS NOT FOUND")

if DEPLOYMENT_MODE == "prod":
    from .settings.production_settings import *
elif DEPLOYMENT_MODE == "preprod":
    from .settings.preprod_settins import *
else:#dev
    from .settings.dev_settings import *