from django.test import TestCase


# Create your tests here.
class BaseTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        #print("setUpTestData: Run once to set up non-modified data for all class methods")
        pass

    def setUp(self):
        # Setup run before every test method.
        pass

    def tearDown(self):
        # Clean up run after every test method.
        pass

    def test_A(self):
        print("\n" + "test A DONE")
        self.assertFalse(False)

    def test_B(self):
        print("\n" + "test B DONE")
        self.assertTrue(True)
